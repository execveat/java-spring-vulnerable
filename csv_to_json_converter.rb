#!/usr/bin/env ruby

require 'csv'
require 'json'
require 'logger'
require 'optparse'
def parse_args
  options = {}
  OptionParser.new do |opts|
    opts.banner = 'Usage: the csv to JSON converter [options] CSV_FILE'
    opts.on('-d', '--debug', 'Run in debug mode')
    opts.on('-c', '--csv CSV', 'The CSV file to conver to JSON format')
    opts.on('-h', '--help', 'Print help message') do
      puts opts
      exit
    end
  end.parse!(into: options)
  options
end

def main(csv_path)
  dict = {
    filter: {
      include: [
        # we only care about the specific test cases. Nothing else.
        "*benchmark/testcode/BenchmarkTest*.java",
      ],
    },
    vulnerabilities: [],
  }

  File.open(csv_path, 'r') do |f|
    csv_file = CSV.new(f, headers: true)
    csv_file.each do |row|
      test_name = row['test name']
      dict[:vulnerabilities].push({
        category: row['category'],
        true_positive: (row['real vulnerability'] == 'true'),
        cwe: row['cwe'].to_i,
        location: {
          file: "src/main/java/org/owasp/benchmark/testcode/#{test_name}.java",
          # we don't have line range information for the owasp benchmark
          start_line: nil,
          end_line: nil,
        },
      })
    end
  end
  puts JSON.pretty_generate(dict)
end

opts = parse_args
main opts[:csv]
